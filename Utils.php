<?php

/**
 * The PHP Utility functions library
 * 
 * @package     PHP Utility Functions Library
 * @version     0.21.4
 * @author      Wayne Davies
 * @copyright   Copyright (C) 2020, Wayne Davies. All rights reserved
 * @lincese     Apache 2.0 http://www.apache.org/licenses/
 */

declare(strict_types=1);

namespace WDF;

/**
 * Note: out() only outputs data if WDF\DEV is TRUE. This
 * suppresses the function in a production environemnt so
 * a live site doesn't display data that shouldn't be seen.
 */
if (!defined('DEV')) define('DEV', TRUE);
if (!defined('QUICK_HASH_TYPE')) define('QUICK_HASH_TYPE', 'sha256');

/**
 * JSON encodes an array containing a code and a string and exits
 */
function ajax_debug (array $output)
{
    if (!isset($output['code'])) die('{"error":"missing status code"}');
    if (!isset($output['msg'])) die('{"error":"missing msg"}');

    echo json_encode($output);
    exit;
}

/**
 * Returns string $data with all punctuation removed.
 */
function depunctuate (string $data): string
{
    return filter_var(
        str_replace(
            [
                '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '=',
                '_', '+', '[', ']', '{', '}', ';', "'", "\\", ':', '"', '|',
                ',', '.', '/', '<', '>', '?', '~'
            ],
            '',
            $data
        ),
        FILTER_SANITIZE_STRING,
        FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH
    );
}

/**
 * Replaces hyphens and underscores with spaces and returns
 */
function deslugify (string $slug): string
{
    $ptn = ['-', '_'];
    $slug = str_replace($ptn, ' ', $slug);
    return $slug;
}

/**
 * Outputs the contents of string $msg and exits.
 */
function fatal (string $msg): void
{
    out('FATAL ERROR: ' . $msg);
    exit;
}

/**
 * Returns the domain portion of the current URL
 */
function get_domain (): string
{
    return $_SERVER['HTTP_HOST'];
}

/**
 * Returns the user's IP address
 */
function get_ip (): string
{
    $ip = $_SERVER['REMOTE_ADDR'];

    if (empty($ip))
    {
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if (!empty($_SERVER['HTTP_CLIENT_IP']))
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        else
            $ip = 'Unknown';
    }

    return $ip;
}

/**
 * Returns the GET parameters on the current URL, or
 * and empty string if none present
 */
function get_params (): string
{
    $raw = explode('?', $_SERVER['REQUEST_URI']);
    return (isset($raw[1]) ? $raw[1] : '');
}

/**
 * Returns http or https as appropriate
 */
function get_protocol (): string
{
    return 'http' . (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 's' : '') . '://';
}

/**
 * Returns the request portion of the current URL,
 * with GET parameters by default. And without if
 * FALSE is requested.
 */
function get_request ($incl_params = TRUE): string
{
    if ($incl_params)
        return $_SERVER['REQUEST_URI'];
    
    return explode('?', $_SERVER['REQUEST_URI'])[0];
}

/**
 * Returns the full URL with GET parameters by default.
 * Without GET parameters if FALSE is passed.
 */
function get_url ($incl_params = TRUE): string
{
    return get_protocol() . get_domain() . get_request($incl_params);
}

/**
 * Returns TRUE if $json contains valid JSON
 */
function is_json (string $json)
{
    $error = '';

    if (
        (substr($json, 0, 1) != '{' && substr($json, 0, 1) != '[') ||
        (substr($json, -1) != '}'   && substr($json, -1) != ']')
    )
    {
        $error = 'Syntax error, malformed JSON';
    }
    else
    {
        $result = json_decode($json);
        switch (json_last_error())
        {
            case JSON_ERROR_NONE:
                $error = '';
                break;
            case JSON_ERROR_DEPTH:
                $error = 'The maximum stack depth has been exceeded';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Invalid or malformed JSON';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Control character error, possibly incorrectly encoded';
                break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON';
                break;
            // PHP >= 5.3.3
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF-8 characters, possibly incorrectly encoded';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_RECURSION:
                $error = 'One or more recursive references in the value to be encoded';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_INF_OR_NAN:
                $error = 'One or more NAN or INF values in the value to be encoded';
                break;
            case JSON_ERROR_UNSUPPORTED_TYPE:
                $error = 'A value of a type that cannot be encoded was given';
                break;
            default:
                $error = 'Unknown JSON error occured';
                break;
        }
    }

    if ($error !== '')
        return $error;

    return TRUE;
}

/**
 * Echos a JSON encoded error and exits if $code is 200,
 * or returns the result if not.
 */
function json_error (int $code, string $msg, string $field = '')
{
    $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
    header($protocol . ' ' . $code);
    //$GLOBALS['http_response_code'] = $code;
    $response = ['code' => $code, 'msg' => $msg];
    if ($field !== '') $response = array_merge($response, ['field' => $field]);
    
    if ($code !== 200)
    {
        echo json_encode($response);
        exit;
    }
    else
        return json_encode($response);
}

/**
 * Outputs a string containing $reps x character $chr
 */
function line ($reps = 80, $chr = '-'): void
{
    echo '<pre>' . str_repeat($chr, $reps) . '</pre>';
}

/**
 * Outputs a variable of any regular type. If a $name
 * is specified the type will also be included.
 */
function out ($data, string $name = '', $backtrace = TRUE): void
{
    $type = gettype($data);
    $class = ($type == 'object' ? 'Instance of ' . get_class($data) : FALSE);

    if ($type === 'array' || $type === 'object')
        $data = print_r($data, TRUE);

    if (is_bool($data))
        $data = ($data === TRUE ? 'TRUE' : 'FALSE');

    if (!empty($name) && defined('DEV'))
        $type = 'const ' . $type;

    if ($class) $type = '';

    $dmpout = '';
    if ($backtrace)
    {
        $dmp = debug_backtrace();
        if (!empty($dmp[0]['line']) && !empty($dmp[0]['file']))
            $dmpout = 'out() called from line ' . $dmp[0]['line'] . ' of ' . $dmp[0]['file'];
    }

    echo '<pre>';
        if ($backtrace === TRUE && $dmpout !== '')
            echo $dmpout . PHP_EOL;

        if ($name !== '')
            echo $type . $class . ' ' . $name . ': ';
        
        echo $data;
    echo '</pre>';
}

/**
 * returns 's' if $value !== 1
 */
function plural (int $value): string
{
    return ($value === 1 ? '' : 's');
}

/**
 * Returns 'y' if $value === 1, and 'ies' if not
 */
function pluralies (int $value): string
{
    return ($value === 1 ? 'y' : 'ies');
}

/**
 * Returns a hash based on a unique randomly generated string
 */
function quick_hash (string $algo = ''): string
{
    $algo = ($algo === '' ? QUICK_HASH_TYPE : $algo);
    return hash($algo, microtime() . bin2hex(random_bytes(24)));
}

/**
 * Header relocates user to $path and exits. Default redirect to home
 */
function redirect (string $path = '/'): void
{
    header('location:' . $path);
    exit;
}

/**
 * Replaces the last instance of $search with $replace in $subject
 */
function str_replacelast (string $search, string $replace, string $subject): string
{
    $pos = strrpos($subject, $search);

    if ($pos !== false)
        $subject = substr_replace($subject, $replace, $pos, strlen($search));

    return $subject;
}

/**
 * Returns a string with no punctuation, all lowercase
 * characters, and with spaces replaced by hyphens.
 */
function slugify (string $data): string
{
    $data = strtolower($data);
    $data = trim(strip_tags($data));
    $data = str_replace(' ', '-', depunctuate($data));
    $data = trim(preg_replace('/--+/m', '-', $data));
    return trim($data, '-');
}