<?php

/**
 * Demonstrates how to load and use of the Utility functions library
 * 
 * @package     index.php
 * @version     0.1.4
 * @author      Wayne Davies
 * @copyright   Copyright (C) 2020, Wayne Davies. All rights reserved
 * @lincese     Apache 2.0 http://www.apache.org/licenses/
 */

declare(strict_types=1);

// Use the WDF namespace aliased to W
use WDF as W;

require 'Utils.php'; // Load the library

$username = 'batman'; // string defined

/**
 * Outputs:
 *     out() called from line 24 of /var/www/html/index.php
 *     const NULL batman: 
 */
W\out($username, 'username');

/**
 * $url will contain the current URL including any GET parameters
 */
$url = W\get_url();
W\out($url);

/**
 * $url will contain the current URL excluding any GET parameters
 */
$url = W\get_url(FALSE);
W\out($url);

/**
 * Outputs bool TRUE returned by is_json()
 */
W\out(W\is_json('{"username":"batman"}'));

/**
 * Outputs string "Syntax error, malformed JSON" returned
 * by function is_json()
 */
W\out(W\is_json('{"username"}'));

/**
 * Outputs:
 *     I have 0 things
 *     I have 1 thing
 *     I have 2 things
 */
$cnt = [0, 1, 2];
foreach ($cnt as $c)
    W\out("I have $c thing" . W\plural($c), '', FALSE);

/**
 * Outputs string returned by quick_hash(), a short and easy
 * way to obtain a quick and unique hash.
 */
W\out(W\quick_hash(), '', FALSE);

/**
 * As above, but overrides the default hash.
 */
W\out(W\quick_hash('md5'), '', FALSE);