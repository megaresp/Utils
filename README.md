# PHP Utility Functions:

The PHP utility functions library is an ultra-lightweight collection that provides developers with simple output, debugging and the means to work with data (including JSON) more easily in a browser.

The library also performs useful tasks such as obtaining URL components, slugifying a string or returning the visitor's IP address.

## Installing:

Simply copy Utils.php to your project and require it. See the attached index.php file for an example.

## Namespace:

The library appears under the namespace WDF. This can be removed if you have no function or constant naming conflicts or aliased as demonstrated in index.php.

## Constants:
### const bool DEV:
If TRUE allows out() to output content. If FALSE out() won't suppress
### const string QUICK_HASH_TYPE:
Sets the hash algorithm used by quick_hash()
## Functions:
### ajax_debug (array $output):
JSON encodes array $output if it contains an int code and a string, and echoes the result.

### depunctuate (string $data): string
Returns string $data with all ASCII punctuation removed, and all non-ASCII characters removed also.

### function deslugify (string $slug): string
Replaces hyphens and underscores with spaces and returns

### function fatal (string $msg): void
Outputs the contents of string $msg and exits the current script.

### function get_domain (): string
Returns the domain portion of the current URL

### function get_ip (): string
Returns the user's IP address

### function get_params (): string
Returns the GET parameters on the current URL, or an empty string if none present

### function get_protocol (): string
Returns http or https as appropriate

### function get_request ($incl_params = TRUE): string
Returns the request portion of the current URL, with GET parameters by default. And without if FALSE is requested.

###function get_url ($incl_params = TRUE): string
Returns the full URL with GET parameters by default. Or without GET parameters if FALSE is passed.

### function is_json (string $json)
Returns bool TRUE if $json contains valid JSON, or a string containing the specific JSON error if not.

### function json_error (int $code, string $msg, string $field = '')
Sets the server response code, echoes a JSON encoded error, and exits if $code is 200 or returns the result if not.

### function line ($reps = 80, $chr = '-'): void
Outputs a string containing $reps x character $chr

### function out ($data, string $name = '', $backtrace = TRUE): void
Outputs a variable of any type along with a backtrace showing which line and file called out(). If a $name is specified data about the type of $data is also included along with the variable's name.

### function plural (int $value): string
returns 's' if $value !== 1

### function pluralies (int $value): string
Returns 'y' if $value === 1, and 'ies' if not

### function quick_hash (string $algo = ''): string
Returns a hash based on a unique randomly generated string. The default hashing algorithm can be overridden by supplying an alternative in the $algo parameter.

### function str_replacelast (string $search, string $replace, string $subject): string
Replaces the last instance of $search with $replace in $subject

### function slugify (string $data): string
Returns a string with any HTML tags removed, no punctuation, all lowercase characters, and with spaces replaced by hyphens.
